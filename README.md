# Duration Formatter

> This simple module provides a field formatter for number fields (integer,
  decimals and floats) which converts seconds into a human-readable format.
  This is useful when extracting the duration of a song into Drupal and it should
  be displayed as readable time format.

## Installation

1. Install the module the [drupal way](http://drupal.org/documentation/install/modules-themes/modules-8)

2. Go to "Manage display" and select _Duration_ or _Responsive Image_ as formatter for a number field.

## Contributing

Pull requests and stars are always welcome. For bugs and feature requests, [please create an issue](https://github.com/yannickoo/duration_formatter/issues/new).
